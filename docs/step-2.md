# Installing Common Packages

### Tools used
- Ansible

## Explaination
Common tools are installed on EC2 instances provisioned in Step-1 using Ansible.

common tools such as:
- NVM – Version 0.33.2
- Node – 8.12.0
- Docker – 18.06 or latest
- Docker Compose – 1.13 or latest
- Openssl – latest version
- Git – latest version

Are installed on the provisioned EC2 instances using Ansible playbook [deployment.yml](https://gitlab.com/_liquid/test-infra-setup/blob/master/ansible/playbooks/deployment.yml)

This Playbook defines a play which uses Ansible role [common](https://gitlab.com/_liquid/test-infra-setup/tree/master/ansible/playbooks/roles/common) to install tools on target operating system