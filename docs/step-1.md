# Setting up EC2 instances on AWS Cloud

### Tools used
- Terraform
- Ansible

## Explaination
Two EC2 instances are provisioned on AWS cloud using [terraform-templates](https://gitlab.com/_liquid/test-infra-setup/tree/master/tf-templates/aws-infra)
[ec2.tf](https://gitlab.com/_liquid/test-infra-setup/blob/master/tf-templates/aws-infra/ec2.tf) creates two EC2 insances with hostname  MSR-test-Instance-1 and MSR-test-Instance-2 with Ubuntu Server 16.04 LTS as a base operating system.

The EC2 instances are tagged so that it can be identified by Ansible dynamic host inventory.

This whole process of provisioning EC2 insances is automated using Ansible playbook [provision.yml](https://gitlab.com/_liquid/test-infra-setup/blob/master/ansible/playbooks/provision.yml)

The Role [deploy_infra](https://gitlab.com/_liquid/test-infra-setup/tree/master/ansible/playbooks/roles/deploy_infra) which is called in the ansible playbook use [Ansible Terraform module](https://docs.ansible.com/ansible/latest/modules/terraform_module.html) to invoke Terraform which then provisions EC2 instances