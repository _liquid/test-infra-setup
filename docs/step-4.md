# Creating Docker container in MSR-test-Instance-2

### Tools used
- Ansible

## Explaination

The Ansible playbook [deployment.yml](https://gitlab.com/_liquid/test-infra-setup/blob/master/ansible/playbooks/deployment.yml) defines a play which uses Ansible role [couchbase](https://gitlab.com/_liquid/test-infra-setup/tree/master/ansible/playbooks/roles/apache) to run Couchbase database inside a Docker container using Docker compose.

The Docker Compose file [docker-compose.yml](https://gitlab.com/_liquid/test-infra-setup/blob/master/couchbase-compose/docker-compose.yml) is used by Ansible to deploy Couchbase database.