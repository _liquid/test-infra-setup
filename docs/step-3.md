# Creating Docker container in MSR-test-Instance-1

### Tools used
- Ansible

## Explaination

The Ansible playbook [deployment.yml](https://gitlab.com/_liquid/test-infra-setup/blob/master/ansible/playbooks/deployment.yml) defines a play which uses Ansible role [apache](https://gitlab.com/_liquid/test-infra-setup/tree/master/ansible/playbooks/roles/apache) to run Apache webserver inside a Docker container using Docker compose.

The Docker Compose file [docker-compose.yml](https://gitlab.com/_liquid/test-infra-setup/blob/master/apache-compose/docker-compose.yml) is used by Ansible to deploy Apache. Apache container will serve files under [static](https://gitlab.com/_liquid/test-infra-setup/tree/master/static) folder in this project