variable "region" {
  default = "us-east-1"
}
variable "key_name" {
  default = "ec2-key"
}

variable "allowed_ip" {
  
}

variable "webserver_cidr" {
  default = "0.0.0.0/0"
}

variable "db_cidr" {
  default = "0.0.0.0/0"
}

variable "webserver_tag_value" {
  
}

variable "database_tag_value" {
  
}

variable "owners" {
  default = ["099720109477"]
}








