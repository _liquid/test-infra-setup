resource "aws_security_group" "webserver-sg" {
    name        = "webserver-sg"
    description = "Security group for apache webserver"
    vpc_id      = "${data.aws_vpc.default.id}"

    ingress {
        from_port   = 8080
        to_port     = 8080
        protocol    = "tcp"
        cidr_blocks = ["${var.webserver_cidr}"]
    }

    # Enabling this port for Ansible to SSH into
    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["${var.allowed_ip}"]
    }

    egress {
      from_port       = 0
      to_port         = 0
      protocol        = "-1"
      cidr_blocks     = ["0.0.0.0/0"]
    }
}

resource "aws_security_group" "couchbase-sg" {
    name        = "couchbase-sg"
    description = "Security group for couchbase database"
    vpc_id      = "${data.aws_vpc.default.id}"

    ingress {
        from_port   = 8091
        to_port     = 8094
        protocol    = "tcp"
        cidr_blocks = ["${var.db_cidr}"]
    }

    ingress {
        from_port   = 11210
        to_port     = 11210
        protocol    = "tcp"
        cidr_blocks = ["${var.db_cidr}"]
    }

    # Enabling this port for Ansible to SSH into
    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["${var.allowed_ip}"]
    }    
    egress {
      from_port       = 0
      to_port         = 0
      protocol        = "-1"
      cidr_blocks     = ["0.0.0.0/0"]
    }
}
resource "aws_instance" "webserver-instance" {
  ami                         = "${data.aws_ami.ubuntu.id}"
  instance_type               = "t2.micro"
  subnet_id                   = "${element(data.aws_subnet_ids.all.ids, 0)}"
  vpc_security_group_ids      = ["${aws_security_group.webserver-sg.id}"]
  associate_public_ip_address = true
  key_name                    = "${var.key_name}"
  tags {
    "role" = "${var.webserver_tag_value}"
  }

  user_data = <<-EOF
                  sudo hostnamectl set-hostname MSR-test-Instance-1
                  sudo reboot
                  EOF
}

resource "aws_instance" "db-instance" {
  ami                         = "${data.aws_ami.ubuntu.id}"
  instance_type               = "t2.micro"
  subnet_id                   = "${element(data.aws_subnet_ids.all.ids, 0)}"
  vpc_security_group_ids      = ["${aws_security_group.couchbase-sg.id}"]
  associate_public_ip_address = true
  key_name                    = "${var.key_name}"
  tags {
    "role" = "${var.database_tag_value}"
  }

  user_data = <<-EOF
                  sudo hostnamectl set-hostname MSR-test-Instance-1
                  sudo reboot
                  EOF

}