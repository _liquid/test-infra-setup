data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "all" {
  vpc_id = "${data.aws_vpc.default.id}"
}

# Fetching Ubuntu Server 16.04 LTS ami id
data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-20181114"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = "${var.owners}"
}
