# Test Infra Setup

This is used to set up Infrastructure on AWS using tools like Terraform, Ansible, Git and Docker 

## Getting Started

### Prerequisites
- [Python](https://www.python.org/downloads) - Download and Install Python
- [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) - Download and Install Ansible
- [Terraform](https://learn.hashicorp.com/terraform/getting-started/install) - Download and Install Terraform
- [PIP](https://pip.pypa.io/en/stable/installing/) - Download and Install pip
- [Boto](http://boto.cloudhackers.com/en/latest/getting_started.html) - Download Boto using pip
- [AWS-CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) - Download and Install AWS Cli
- [EC2-Key-Pair](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html) - Skip this step if you wish to use an existing key-pair.

### Setting up Environment Variables
Set up environment variables using below commands 
```
export AWS_ACCESS_KEY_ID='your access key id'
export AWS_SECRET_ACCESS_KEY='your secret access key'
export ANSIBLE_HOST_KEY_CHECKING=False
```

### Provisioning EC2 Instance on AWS
Run below command from this projects root directory to deploy EC2 instance on AWS Cloud:
```
ansible-playbook -i ./ansible/hosts --private-key <path to key-pair> -e "allowed_ip=<your ip address>/32" ./ansible/playbooks/provision.yml
```

### Setting up Apache Webserver and Couchbase Database
Run below command from this projects root directory to deploy Apache webserver and Coouchbase database on provisioned EC2 instance
```
ansible-playbook -i ./ansible/hosts --private-key <path-to-key-pair> ./ansible/playbooks/deployment.yml
```